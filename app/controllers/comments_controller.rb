class CommentsController < ApplicationController
    def create
        @comment = Comment.new
        @comment.title = params[:input_title]
        @comment.content = params[:input_content]
        @comment.post_id = params[:post_id]
        @comment.save
        redirect_to "/posts/#{params[:post_id]}"
    end
    
    def destroy
        @comment = Comment.find()
        @comment.destroy
        redirect_to "/posts/#{params[:post_id]}"
    end
end
